$java = @"
package a.b.c;

//import org.bson.codecs.pojo.annotations.BsonDiscriminator;
import u.v.W;
import x.y.Z;

@Ann1
public class M extends N {

}
"@

function getMatch1([String] $match, [String] $text) {
    $text -match $match > $null
    if ($Matches.Count -gt 1) {
        return $Matches[1]
    } else {
        return ''
    }
}

function hasMatch1([String] $match, [String] $text) {
    $text -match $match
}

function replaceMatch1([Regex] $match, [String] $replacement, [String] $text) {
    $match.replace($text, $replacement, 1)
}

function insertBsonDiscriminatorClassAnnotation([String] $java) {
    $import = 'import org.bson.codecs.pojo.annotations.BsonDiscriminator;'
    if (hasMatch1 -match "`n\s*$import" -text $java) {
        return '' #$java # already inserted, do not modify source
    }
    $package = getMatch1 -match '^\s*package\s+(.+);' -text $java
    $class = getMatch1 -match "`n\s*public\s+class\s+(.+)\s+extends" -text $java
    if ($package -eq '' -or $class -eq '') {
        return '' #$java # not a model class, do not modify source
    }
    $annotation= "@BsonDiscriminator(key=`"type`", value=`"$package.$class`")"
    $javaWithImport = replaceMatch1 -match "`n\s*import\s+" -replacement "`n`n$import`n`nimport " -text $java
    $javaWithAnnotation = replaceMatch1 -match "`n\s*public\s+class\s+" -replacement "`n$annotation`npublic class " -text $javaWithImport
    return $javaWithAnnotation
}

# import lombok.Data;
# import lombok.EqualsAndHashCode;
# import lombok.NoArgsConstructor;
# import lombok.ToString;

# @Data
# @NoArgsConstructor
# @EqualsAndHashCode(callSuper=true)
# @ToString(callSuper=true)

function insertClassAnnotations([String] $path) {
    Get-ChildItem -Recurse -File -Path $path | ForEach-Object {
        $java = Get-Content -Path $_.FullName -Raw
        $updatedJava = insertBsonDiscriminatorClassAnnotation -java $java
        if ($updatedJava -ne '') {
            Set-Content -Path $_.FullName -Value $updatedJava
        }
    }
}

#insertClassAnnotation -java $java
#insertClassAnnotations -path '/Users/eoinomeara/IC4/PROTECTIVE/ca/ca-mair-api-model/src/main/java/eu/h2020/protective/ca/mair/api/model2'
insertClassAnnotations -path '/Users/eoinomeara/IC4/PROTECTIVE/ca/ca-mair-api-model/src/main/java/eu/h2020/protective/ca/mair/api/model'