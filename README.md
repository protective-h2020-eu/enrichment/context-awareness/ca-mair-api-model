PROTECTIVE  Context Awareness (CA) - MAIR API Model
========== 

`ca-mair-api-model` is a Java library that is included in `ca-mair` and `ca-fusioninventory`.

Information on those two products can be found in the `README.md` of the `ca-mair` project located at `https://gitlab.com/protective-h2020-eu/enrichment/context-awareness/ca-mair.git`

Building this jar
--------
In order to build this jar 
- clone this repository
- checkout the appropriate branch, only the master branch is being delivered!
- on Linux/macOS run `./gradlew build` in a terminal
- on Windows run `gradlew.bat build` at the command prompt
