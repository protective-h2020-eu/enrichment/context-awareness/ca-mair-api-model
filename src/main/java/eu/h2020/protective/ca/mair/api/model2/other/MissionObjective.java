package eu.h2020.protective.ca.mair.api.model2.other;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@ApiModel
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model2.other.MissionObjective")
public class MissionObjective extends Vertex {

	public MissionObjective(String key) throws Exception {
		super(key);
		setWeight(1.0d);
	}

}

