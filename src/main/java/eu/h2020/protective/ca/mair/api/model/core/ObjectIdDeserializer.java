package eu.h2020.protective.ca.mair.api.model.core;

import java.io.IOException;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class ObjectIdDeserializer extends JsonDeserializer<ObjectId> {

    @Override
    public ObjectId deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
    	// Ignore date, time, timeSecond in JSON.
		JsonNode objectId = ((JsonNode)parser.readValueAsTree());       
        int timestamp = objectId.get("timestamp").intValue();
        int machineIdentifier = objectId.get("machineIdentifier").intValue();
        short processIdentifier = objectId.get("processIdentifier").shortValue();
        int counter = objectId.get("counter").intValue();
        return new ObjectId(timestamp, machineIdentifier, processIdentifier, counter);
    }

}