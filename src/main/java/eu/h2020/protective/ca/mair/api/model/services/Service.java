package eu.h2020.protective.ca.mair.api.model.services;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ApiModel
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.services.Service")
public class Service extends Vertex {

	@ApiModelProperty(required = false, value = "Location of Service")
	private String location;
	
	@ApiModelProperty(required = false, value = "Person responsible for technical support of Service")
	private String technicalOwner;
	
	@ApiModelProperty(required = false, value = "Person responsible for use of Service")
	private String businessOwner;
	
	@ApiModelProperty(required = false, value = "Description of Service")
	private String description;

	public Service(String key) {
		super(key);
	}

}

