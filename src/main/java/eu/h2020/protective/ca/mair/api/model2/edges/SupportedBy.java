package eu.h2020.protective.ca.mair.api.model2.edges;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model2.assets.Information;
import eu.h2020.protective.ca.mair.api.model2.other.MissionObjective;
import eu.h2020.protective.ca.mair.api.model2.services.BusinessProcess;
import eu.h2020.protective.ca.mair.api.model2.services.ITService;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@ApiModel
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model2.edges.SupportedBy")
public class SupportedBy extends Edge {

	public SupportedBy(MissionObjective from, BusinessProcess to) {
		super(from, to);
	}

	public SupportedBy(MissionObjective from, ITService to) {
		super(from, to);
	}

	public SupportedBy(MissionObjective from, Information to) {
		super(from, to);
	}

}

