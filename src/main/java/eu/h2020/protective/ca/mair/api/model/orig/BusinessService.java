package eu.h2020.protective.ca.mair.api.model.orig;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.core.Vertex;
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.orig.BusinessService")
public class BusinessService extends Vertex {

	public BusinessService() {
		super();
	}

	public BusinessService(String key) {
		super(key);
	}

}

