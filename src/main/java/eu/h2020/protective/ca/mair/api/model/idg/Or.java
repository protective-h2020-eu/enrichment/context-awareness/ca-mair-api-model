package eu.h2020.protective.ca.mair.api.model.idg;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.core.Vertex;
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.idg.Or")
public class Or extends Vertex {

	public Or() {
		super();
		setExpression(this.getClass().getSimpleName().toUpperCase());
	}
	
	public Or(String key) {
		this();
		setKey(key);
	}
	
}

