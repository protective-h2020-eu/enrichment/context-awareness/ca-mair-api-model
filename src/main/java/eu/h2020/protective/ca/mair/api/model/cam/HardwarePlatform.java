package eu.h2020.protective.ca.mair.api.model.cam;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.cam.HardwarePlatform")
public class HardwarePlatform extends Vertex {

	public HardwarePlatform(String key) {
		super(key);
	}

}

