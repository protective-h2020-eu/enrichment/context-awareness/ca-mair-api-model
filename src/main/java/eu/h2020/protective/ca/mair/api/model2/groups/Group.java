package eu.h2020.protective.ca.mair.api.model2.groups;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import java.util.Set;

import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@ApiModel(value = "Group")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model2.groups.Group<T extends Vertex>")
public class Group<T extends Vertex> extends Vertex {

	@ApiModelProperty(required = false, value = "Location of Group")
	private String location;
	
	@ApiModelProperty(required = false, value = "Person responsible for technical support of Group")
	private String technicalOwner;
	
	@ApiModelProperty(required = false, value = "Person responsible for use of Group")
	private String businessOwner;
	
	@ApiModelProperty(required = false, value = "Description of Group")
	private String description;
	
	@ApiModelProperty(required = false, value = "Items in Group")
	private Set<T> items;
	
	public Group() {
		super();
	}

	public Group(String key) {
		super(key);
	}

}

