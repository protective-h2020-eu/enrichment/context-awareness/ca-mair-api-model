package eu.h2020.protective.ca.mair.api.model.cam;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.cam.ImpactFactor")
public class ImpactFactor extends Vertex {

	public ImpactFactor(String key, Double weight) throws Exception {
		super(key, weight);
	}

	@Override
	public void setWeight(Double weight) {
		super.checkWeight(weight, 0.0, 1.0);
		super.setWeight(weight);
	}

}

