package eu.h2020.protective.ca.mair.api.model.assets;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel(value = "Computer")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.assets.Computer")
public class Computer extends Device {
	
	public Computer() {
		super();
	}

	public Computer(String key) {
		super(key);
	}

}

