package eu.h2020.protective.ca.mair.api.model.core;

import java.io.Serializable;
import java.util.List;
import java.util.function.Supplier;

public class EdgeSupplier implements Supplier<Edge>, Serializable {

	private static final long serialVersionUID = -422108158328888508L;

	private List<Edge> edges;
	private int i = 0;

	public EdgeSupplier(List<Edge> edges, int start) {
		this.edges = edges;
		this.i = start;
	}

	@Override
	public Edge get() {
		return edges.get(i++);
	}
	
	/**
	 * Create an Edge supplier which returns a sequence starting from a specific
	 * numbers.
	 * 
	 * @param edges
	 *            List of Edges
	 * @param start
	 *            where to start the sequence
	 * @return an Edge supplier
	 */
	public static Supplier<Edge> createSupplier(List<Edge> edges, int start) {
		return new EdgeSupplier(edges, start);
	}

}
