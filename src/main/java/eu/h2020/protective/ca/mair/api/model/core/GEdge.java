package eu.h2020.protective.ca.mair.api.model.core;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false, exclude = { "edge" })
public class GEdge {

	private String key;
	
	private String fromCdg;
	
	private String toCdg;
	
	private Edge edge;
	
	public GEdge() {
		super();
	}

	public GEdge(String key) {
		this();
		setKey(key);
	}

	public GEdge(String key, Edge edge) {
		this(key);
		setEdge(edge);
		setFromCdg(edge.getFromCdg());
		setToCdg(edge.getToCdg());
	}

}
