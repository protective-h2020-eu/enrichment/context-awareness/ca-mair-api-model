package eu.h2020.protective.ca.mair.api.model.assets;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel(value = "OperatingSystem")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.assets.OperatingSystem")
public class OperatingSystem extends Software {

	@ApiModelProperty(required = false, value = "The full name of the Operating System")
	private String fullName;

	@ApiModelProperty(required = false, value = "The name of the Operating System kernel")
	private String kernelName;

	@ApiModelProperty(required = false, value = "The version of the Operating System kernel")
	private String kernelVersion;

	@ApiModelProperty(required = false, value = "Comments about this Operating System instance")
	private String comment;

	public OperatingSystem() {
		super();
	}

	public OperatingSystem(String key) {
		super(key);
	}

}

