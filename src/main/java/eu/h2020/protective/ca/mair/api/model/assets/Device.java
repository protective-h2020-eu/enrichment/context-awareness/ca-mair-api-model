package eu.h2020.protective.ca.mair.api.model.assets;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel(value = "Device")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.assets.Device")
public class Device extends Asset {

	@ApiModelProperty(required = false, value = "The device identifier")
	private String deviceId;

	@ApiModelProperty(required = false, value = "Primary IPv4 or IPv6 Addresses")
	private List<String> ipAddresses = new ArrayList<>();

	@ApiModelProperty(required = false, value = "MAC Address")
	private String macAddress;

	@ApiModelProperty(required = false, value = "Default IP Gateway")
	private String defaultGateway;

	@ApiModelProperty(required = false, value = "List of IP Gateways")
	private String ipGateway;

	@ApiModelProperty(required = false, value = "DNS")
	private String dns;

	@ApiModelProperty(required = false, value = "IPv4 subnet")
	private String ipV4Subnet;

	@ApiModelProperty(required = false, value = "IPv6 subnet")
	private String ipV6Subnet;

	@ApiModelProperty(required = false, value = "IPv4 subnet mask")
	private String ipV4SubnetMask;

	@ApiModelProperty(required = false, value = "IPv6 subnet mask")
	private String ipV6SubnetMask;

	@ApiModelProperty(required = false, value = "IPv4 addresses")
	private List<String> ipV4Addresses = new ArrayList<>();

	@ApiModelProperty(required = false, value = "IPv6 addresses")
	private List<String> ipV6Addresses = new ArrayList<>();

	@ApiModelProperty(required = false, value = "'Wifi' and/or 'Ethernet'")
	private String networkType;

	@ApiModelProperty(required = false, value = "Model name")
	private String model;
	
	// @ApiModelProperty(required = false, value = "Operating System instance")
	// private OperatingSystem os;
	
	@ApiModelProperty(required = false, value = "Physical ID of Asset in organisation")
	private String assetTag;
	
	@ApiModelProperty(required = false, value = "'Real' or 'Virtual' device")
	private String deviceType;
	
	@ApiModelProperty(required = false, value = "e.g. x86, x64, amd64, arm, etc.")
	private String architecture;

	public Device() {
		super();
	}

	public Device(String key) {
		super(key);
	}

}

