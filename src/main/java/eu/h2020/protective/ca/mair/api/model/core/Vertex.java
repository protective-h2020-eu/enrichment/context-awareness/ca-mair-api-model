package eu.h2020.protective.ca.mair.api.model.core;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;
import org.bson.codecs.pojo.annotations.BsonIgnore;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data // FIXME Include (not exclude) score if possible.
@EqualsAndHashCode(callSuper = true, exclude = { "cdgCount" })
@ToString(callSuper = true)
@JsonNaming() // use default naming strategy instead of naming strategy of sub class
@ApiModel(value = "Vertex", parent = GraphElement.class)
@BsonDiscriminator(key = "type", value = "eu.h2020.protective.ca.mair.api.model.core.Vertex")
public class Vertex extends GraphElement {

	public static final String AND = "AND";
	public static final String OR = "OR";
	public static final String SINK = "SINK";
	public static final String ERROR = "Error!";

	@ApiModelProperty(required = false, value = "AND, OR, SINK")
	private String expression = OR;

	private int cdgCount = 0;
	
	private double score = 0.0d;
	
	public void setExpression(String expression) {
		switch (expression) {
		case Vertex.AND:
		case Vertex.OR:
		case Vertex.SINK:
			this.expression = expression;
			break;
		default:
			this.expression = ERROR;
			break;
		}
	}
	
	@JsonIgnore
	@BsonIgnore
	public GVertex getGVertex() {
		return new GVertex(this.getKey(), this);
	}

	public int incrementCdgCount() {
		if (cdgCount < Integer.MAX_VALUE) {
			cdgCount++;
		}
		return cdgCount;
	}

	public int decrementCdgCount() {
		if (cdgCount > 0) {
			cdgCount--;
		}
		return cdgCount;
	}

	public Vertex() {
		super();
	}

	public Vertex(String key) {
		this();
		setKey(key);
	}

	public Vertex(String key, String type) {
		this(key);
		setType(type);
	}

	public Vertex(String key, Double weight) throws Exception {
		this(key);
		setWeight(weight);
	}

	public Vertex(String key, String type, Double weight) throws Exception {
		this(key, type);
		setWeight(weight);
	}

}
