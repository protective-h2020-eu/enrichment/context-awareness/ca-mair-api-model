package eu.h2020.protective.ca.mair.api.model2.edges;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.api.model2.nodes.VirtualMachine;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@ApiModel
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model2.edges.RunsOn")
public class RunsOn extends Edge {

	public RunsOn(VirtualMachine from, Software to) {
		super(from, to);
	}

	public RunsOn(Software from, Node to) {
		super(from, to);
	}

	public RunsOn(Node from, Node to) {
		super(from, to);
	}

}

