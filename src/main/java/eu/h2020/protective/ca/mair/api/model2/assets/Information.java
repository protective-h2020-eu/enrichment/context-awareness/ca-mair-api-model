package eu.h2020.protective.ca.mair.api.model2.assets;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@ApiModel(value = "Information")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model2.assets.Information")
public class Information extends Asset {

	public Information() {
		super();
	}

	public Information(String key) {
		super(key);
	}

}

