package eu.h2020.protective.ca.mair.api.model.services;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ApiModel
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.services.ConnectivityService")
public class ConnectivityService extends Service {

	public ConnectivityService(String key) {
		super(key);
	}

}

