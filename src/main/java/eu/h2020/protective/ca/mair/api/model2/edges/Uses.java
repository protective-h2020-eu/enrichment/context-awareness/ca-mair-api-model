package eu.h2020.protective.ca.mair.api.model2.edges;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model2.assets.Information;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.api.model2.other.User;
import eu.h2020.protective.ca.mair.api.model2.services.BusinessProcess;
import eu.h2020.protective.ca.mair.api.model2.services.ITService;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@ApiModel
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model2.edges.Uses")
public class Uses extends Edge {

	public Uses(User from, Node to) {
		super(from, to);
	}

	public Uses(User from, Software to) {
		super(from, to);
	}

	public Uses(Software from, Software to) {
		super(from, to);
	}

	public Uses(ITService from, ITService to) {
		super(from, to);
	}

	public Uses(BusinessProcess from, Information to) {
		super(from, to);
	}

	public Uses(BusinessProcess from, ITService to) {
		super(from, to);
	}

}

