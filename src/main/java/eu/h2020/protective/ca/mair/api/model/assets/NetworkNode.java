package eu.h2020.protective.ca.mair.api.model.assets;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel(value = "NetworkNode")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.assets.NetworkNode")
public class NetworkNode extends Device {

	public NetworkNode() {
		super();
	}

	public NetworkNode(String key) {
		super(key);
	}

}

