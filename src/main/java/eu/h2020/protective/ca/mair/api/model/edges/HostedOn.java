package eu.h2020.protective.ca.mair.api.model.edges;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.assets.Application;
import eu.h2020.protective.ca.mair.api.model.assets.Device;
import eu.h2020.protective.ca.mair.api.model.assets.OperatingSystem;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ApiModel
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.edges.HostedOn")
public class HostedOn extends Edge {

	public HostedOn(OperatingSystem from, Device to) { // TODO inverse of runs on
		super(from, to);
	}

	public HostedOn(Device from, OperatingSystem to) { // TODO inverse of runs on
		super(from, to);
	}

	public HostedOn(Application from, OperatingSystem to) { // TODO inverse of runs on
		super(from, to);
	}

}

