package eu.h2020.protective.ca.mair.api.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DescriptionValidator implements ConstraintValidator<ApiDescription, String> {

	@Override
	public void initialize(ApiDescription constraintAnnotation) {
	}

	@Override
	public boolean isValid(String object, ConstraintValidatorContext constraintContext) {
		return true;
	}
	
}