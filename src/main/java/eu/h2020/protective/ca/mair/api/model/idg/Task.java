package eu.h2020.protective.ca.mair.api.model.idg;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.core.Vertex;

@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.idg.Task")
public class Task extends Vertex {

	public Task() {
		super();
	}
	
	public Task(String key) {
		super(key);
	}

}

