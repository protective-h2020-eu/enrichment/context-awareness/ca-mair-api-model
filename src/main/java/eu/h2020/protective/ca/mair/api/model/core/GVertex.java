package eu.h2020.protective.ca.mair.api.model.core;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false, exclude = { "vertex" })
public class GVertex {

	private String key;
	
	private Vertex vertex;
	
	public GVertex() {
		super();
	}

	public GVertex(String key) {
		this();
		setKey(key);
	}

	public GVertex(String key, Vertex vertex) {
		this(key);
		setVertex(vertex);
	}

}
