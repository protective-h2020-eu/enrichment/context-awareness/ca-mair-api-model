package eu.h2020.protective.ca.mair.api.model2.groups;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model2.assets.Asset;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@ApiModel(value = "Pool")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model2.groups.Pool")
public class Pool extends Group<Asset> {

	public Pool() {
		super();
	}

	public Pool(String key) {
		super(key);
	}

}

