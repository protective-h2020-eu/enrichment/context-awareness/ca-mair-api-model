package eu.h2020.protective.ca.mair.api.model2.groups;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@ApiModel(value = "Cluster")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model2.groups.Cluster")
public class Cluster extends Group<Node> {

	public Cluster() {
		super();
	}

	public Cluster(String key) {
		super(key);
	}

}

