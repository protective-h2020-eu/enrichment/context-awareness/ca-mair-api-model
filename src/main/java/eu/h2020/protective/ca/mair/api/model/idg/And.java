package eu.h2020.protective.ca.mair.api.model.idg;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.core.Vertex;
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.idg.And")
public class And extends Vertex {

	public And() {
		super();
		setExpression(this.getClass().getSimpleName().toUpperCase());
	}

	public And(String key) {
		this();
		setKey(key);
	}
	
}

