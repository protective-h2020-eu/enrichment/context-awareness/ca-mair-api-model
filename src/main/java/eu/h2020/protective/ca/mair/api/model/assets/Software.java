package eu.h2020.protective.ca.mair.api.model.assets;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel(value = "Software")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.assets.Software")
public class Software extends Asset {

	@ApiModelProperty(required = false, value = "Software version")
	private String version;
	
	@ApiModelProperty(required = false, value = "Software patch level")
	private String patchLevel;
	
//	@ApiModelProperty(required = false, value = "   ")
//	private String licenseKey;
	
	@ApiModelProperty(required = false, value = "e.g. x86, x64, amd64, arm, etc.")
	private String targetArchitecture;

	public Software() {
		super();
	}

	public Software(String key) {
		super(key);
	}

}

