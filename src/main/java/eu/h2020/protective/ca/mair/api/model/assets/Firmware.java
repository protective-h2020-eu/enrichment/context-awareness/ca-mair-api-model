package eu.h2020.protective.ca.mair.api.model.assets;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
//@ApiModel(value = "Firmware")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.assets.Firmware")
public class Firmware extends Software {

	public Firmware() {
		super();
	}

	public Firmware(String key) {
		super(key);
	}

}

