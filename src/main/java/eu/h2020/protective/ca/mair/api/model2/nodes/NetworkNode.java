package eu.h2020.protective.ca.mair.api.model2.nodes;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@ApiModel(value = "NetworkNode")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model2.nodes.NetworkNode")
public class NetworkNode extends Node {

	public NetworkNode() {
		super();
	}

	public NetworkNode(String key) {
		super(key);
	}

}

