package eu.h2020.protective.ca.mair.api.model.edges;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.assets.Device;
import eu.h2020.protective.ca.mair.api.model.assets.Software;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.other.User;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ApiModel
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.edges.Uses")
public class Uses extends Edge {

	public Uses(User from, Device to) { // TODO inverse of uses on
		super(from, to);
	}

	public Uses(User from, Software to) { // TODO inverse of uses on
		super(from, to);
	}

}

