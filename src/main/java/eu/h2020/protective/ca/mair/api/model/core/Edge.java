package eu.h2020.protective.ca.mair.api.model.core;

import java.util.Arrays;
import java.util.List;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;
import org.bson.codecs.pojo.annotations.BsonIgnore;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonNaming() // use default naming strategy instead of naming strategy of sub class
@ApiModel(value = "Edge", parent = GraphElement.class)
@BsonDiscriminator(key = "type", value = "eu.h2020.protective.ca.mair.api.model.core.Edge")
public class Edge extends GraphElement {

	@ApiModelProperty(required = true, value = "Name of 'CDG' the Edge links from")
	private String fromCdg;

	@ApiModelProperty(required = true, value = "Name of 'CDG' the Edge links to")
	private String toCdg;

	@ApiModelProperty(required = true, value = "Internal ID of 'from' Vertex (auto-generated)")
	@JsonProperty(value = "source")
	private String from;

	@ApiModelProperty(required = true, value = "Internal ID of 'to' Vertex (auto-generated)")
	@JsonProperty(value = "target")
	private String to;
	
	@ApiModelProperty(required = true, value = "Simple Type of 'from' Vertex (auto-generated)")
	@JsonProperty(value = "sourceType")
	private String fromType;

	@ApiModelProperty(required = true, value = "Simple Type of 'to' Vertex (auto-generated)")
	@JsonProperty(value = "targetType")
	private String toType;

	@JsonIgnore
	@BsonIgnore
	private Vertex source;

	@JsonIgnore
	@BsonIgnore
	private Vertex target;

	@ApiModelProperty(required = true, value = "List of internal IDs of Vertices (auto-generated)")
	private List<String> vertices;

//	private int value = 1; // TODO is this used by NetworkX or JGraphT

	public static final String ALL_EDGES_CDG = "{{allEdgesCDG}}";
	public static final String OTHER_EDGES_CDG = "{{otherEdgesCDG}}";
	public static final String SINK_CDG = "{{sinkCDG}}";

	@JsonIgnore
	@BsonIgnore
	public GVertex getGSource() {
		return new GVertex(source.getKey(), source);
	}

	@JsonIgnore
	@BsonIgnore
	public GVertex getGTarget() {
		return new GVertex(target.getKey(), target);
	}

	@JsonIgnore
	@BsonIgnore
	public GEdge getGEdge() {
		return new GEdge(this.getKey(), this);
	}

	public void setFrom(String from) {
		this.from = from;
		this.vertices.set(0, from);
	}

	public void setTo(String to) {
		this.to = to;
		this.vertices.set(1, to);
	}

	public void setVertices(List<String> vertices) {
		this.from = vertices.get(0);
		this.to = vertices.get(1);
		this.vertices = vertices;
	}

	@JsonIgnore
	@BsonIgnore
	public boolean isInterLayerDependency() {
		return !fromCdg.equals(toCdg);
	}

	@JsonIgnore
	@BsonIgnore
	public boolean isLoopDependency() {
		return from.equals(to);
	}

	public Edge() {
		super();
		fromCdg = OTHER_EDGES_CDG;
		toCdg = OTHER_EDGES_CDG;
		from = "";
		to = "";
		vertices = Arrays.asList(from, to);
	}

	public Edge(String from, String to) {
		this(from + "-->" + to, from, to);
	}

	private Edge(String key, String from, String to) {
		this();
		setFrom(from);
		setTo(to);
		setKey(key);
	}

	public Edge(Vertex from, Vertex to) {
		this(from.getKey(), to.getKey());
		setSource(from);
		setTarget(to);
	}

	public Edge(Vertex from, Vertex to, Double weight) throws Exception {
		this(from, to);
		setWeight(weight);
	}

	public Edge(String key, Vertex from, Vertex to) {
		this(from, to);
		setKey(key);
	}

	public Edge(String key, Vertex from, Vertex to, Double weight) throws Exception {
		this(from, to, weight);
		setKey(key);
	}

	public Edge changeDirection() {
		String oldFrom = getFrom();
		String oldTo = getTo();
		setFrom(oldTo);
		setTo(oldFrom);
		Vertex oldSource = getSource();
		Vertex oldTarget = getTarget();
		setSource(oldTarget);
		setTarget(oldSource);
		vertices = Arrays.asList(oldTo, oldFrom);
		return this;
	}

}
