package eu.h2020.protective.ca.mair.api.model.core;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;

import javax.validation.constraints.NotBlank;

import org.jgrapht.graph.DirectedWeightedPseudograph; // TODO change this for performance?

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import eu.h2020.protective.ca.mair.api.constraints.ApiDescription;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true, value = { "vertexSupplier", "edgeSupplier", "edgeFactory", "type" })
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = false)
@ApiModel(description = "Graph or sub-graph.")
public class Graph extends DirectedWeightedPseudograph<GVertex, GEdge> {

	private static final long serialVersionUID = 810572260727235056L;

	private NetXGraph graph = new NetXGraph();
	
//	private String lastLocalCpeUpdateTime;
	public static String lastLocalCpeUpdateTime;

	@NotBlank
	@ApiDescription("The context to which this graph belongs")
	@ApiModelProperty(required = true, value = "The context to which this graph belongs")
	private String context;

	@ApiDescription("List of different subclasses that extend Vertex. Only common fields shown here.")
	@ApiModelProperty(required = false, value = "List of different subclasses that extend Vertex.")
	@JsonProperty(value = "nodes")
	private List<Vertex> vertices;

	@ApiDescription("List of different subclasses that extend Edge. Only common fields shown here.")
	@ApiModelProperty(required = false, value = "List of different subclasses that extend Edge.")
	@JsonProperty(value = "links")
	private List<Edge> edges;
	
//	private final Set<GVertex> vertexSet = new TreeSet<>(Comparator.comparing(v -> v.getKey()));

	public List<Vertex> getVertices() {
		return Collections.unmodifiableList(vertices);
	}

	public List<Edge> getEdges() {
		return Collections.unmodifiableList(edges);
	}

	public List<Edge> clearEdges() {
		edges.clear();
		return getEdges();
	}

	public boolean addVertex(Vertex v) {
		return this.addVertex(v.getGVertex());
	}

	public boolean addEdge(Vertex from, Vertex to, Edge e) {
		return this.addEdge(from.getGVertex(), to.getGVertex(), e.getGEdge());
	}

    /**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addVertex(GVertex v) {
		if (super.addVertex(v)) {
			vertices.add(v.getVertex());
			return true;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addEdge(GVertex sourceVertex, GVertex targetVertex, GEdge e) {
		e.getEdge().setToType(sourceVertex.getVertex().getClass().getSimpleName());
		e.getEdge().setFromType(targetVertex.getVertex().getClass().getSimpleName());
		if (super.addEdge(sourceVertex, targetVertex, e)) {
			edges.add(e.getEdge());
			return true;
		}
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removeVertex(GVertex v) {
		if (super.removeVertex(v)) {
			vertices.remove(v.getVertex());
			return true;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removeEdge(GEdge e) {
		if (super.removeEdge(e)) {
			edges.remove(e.getEdge());
			return true;
		}
		return false;
	}

	public Graph() {
		this(new ArrayList<>(), new ArrayList<>());
	}

	private Graph(List<Vertex> vertices, List<Edge> edges) {
		super(null, null);
		setVertices(vertices);
		setEdges(edges);
	}
	
	public void orderVertices() {
		vertices.sort(Comparator.comparing(v -> v.getKey()));
	}

	public void orderEdges() {
		edges.sort(Comparator.comparing(v -> v.getKey()));
	}

	public String toJson() throws JsonProcessingException {
		final TimeZone tz = TimeZone.getTimeZone("GMT");
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		final ObjectWriter ow = new ObjectMapper().writer().with(tz).with(df).withDefaultPrettyPrinter();
		orderVertices();
		orderEdges();
		final String json = ow.writeValueAsString(this);
		return json;
	}

	public Path toJsonFile(final String path) throws JsonProcessingException, IOException {
		return Files.write(Paths.get(path), this.toJson().getBytes());
	}

}
