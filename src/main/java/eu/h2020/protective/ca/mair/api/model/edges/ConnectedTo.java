package eu.h2020.protective.ca.mair.api.model.edges;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.assets.Asset;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ApiModel
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.edges.ConnectedTo")
public class ConnectedTo extends Edge {

	public ConnectedTo(Asset from, Asset to) {
		super(from, to);
	}

}

