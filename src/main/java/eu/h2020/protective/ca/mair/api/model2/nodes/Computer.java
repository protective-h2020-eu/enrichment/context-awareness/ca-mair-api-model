package eu.h2020.protective.ca.mair.api.model2.nodes;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@ApiModel(value = "Computer")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model2.nodes.Computer")
public class Computer extends Node {
	
	public Computer() {
		super();
	}

	public Computer(String key) {
		super(key);
	}

}

