package eu.h2020.protective.ca.mair.api.model.core;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import eu.h2020.protective.ca.mair.api.constraints.ApiDescription;
import eu.h2020.protective.ca.mair.api.model.assets.Application;
import eu.h2020.protective.ca.mair.api.model.assets.Asset;
import eu.h2020.protective.ca.mair.api.model.assets.Computer;
import eu.h2020.protective.ca.mair.api.model.assets.Device;
import eu.h2020.protective.ca.mair.api.model.assets.Firmware;
import eu.h2020.protective.ca.mair.api.model.assets.Information;
import eu.h2020.protective.ca.mair.api.model.assets.NetworkNode;
import eu.h2020.protective.ca.mair.api.model.assets.OperatingSystem;
import eu.h2020.protective.ca.mair.api.model.assets.Software;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Data // FIXME Include (not exclude) weight if possible
@EqualsAndHashCode(callSuper = false, exclude = { "id", "created", "lastModified" })
@ApiModel(value = "GraphElement", discriminator = "type", subTypes = { Vertex.class,

		Application.class, Asset.class, Computer.class, Device.class, Firmware.class, Information.class,
		NetworkNode.class, OperatingSystem.class, Software.class,

		Edge.class })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "type")
@BsonDiscriminator(key = "type", value = "eu.h2020.protective.ca.mair.api.model.core.GraphElement")
public abstract class GraphElement {

	@ApiDescription("Internal ID used by local DB (auto-generated)")
	@ApiModelProperty(required = false, value = "Internal ID used by local DB (auto-generated)")
	@JsonProperty(value = "_id")
	@JsonDeserialize(using = ObjectIdDeserializer.class)
	private ObjectId id;

	@NotBlank
	@ApiDescription("A globally unique ID independent of the DB")
	@ApiModelProperty(required = true, value = "A globally unique ID independent of the DB")
	@JsonProperty(value = "id")
	private String key;

	@ApiDescription("A descriptive name for the instance")
	@ApiModelProperty(required = true, value = "A descriptive name for the instance")
	private String name;

	/**
	 * The full name of the class.
	 */
	@NotBlank
	@ApiDescription("The full name of the class")
	@ApiModelProperty(required = true, value = "The full name of the class")
	@JsonIgnore
	private String type;

	/**
	 * The full name of the class.
	 */
	@NotBlank
	@ApiDescription("The simple name of the class")
	@ApiModelProperty(required = true, value = "The simple name of the class")
	private String simpleType;

	/**
	 * The weighting representing operational capacity.
	 */
	@ApiDescription("The weighting representing operational capacity")
	@ApiModelProperty(required = false, value = "The weighting representing operational capacity")
	private Double weight;

	@ApiModelProperty(required = false, value = "'none' | 'low' | 'medium' | 'high' | 'critical'")
	@JsonProperty(value = "weightString")
	private String grade = "none";
	
	@JsonIgnore
	@BsonIgnore
	private Integer group; // For backwards compatibility, no longer used.

	// @CreatedDate
	@JsonIgnore
	private Date created;

	// @LastModifiedDate
	@JsonIgnore
	private Date lastModified;

	@Getter
	@JsonIgnore
	@BsonIgnore
	private static final List<String> allowedGrades = Arrays.asList("\"none\"", "\"low\"", "\"medium\"", "\"high\"",
			"\"critical\"");

	public static final String NO_GRADE = "";

	public static boolean isValidGrade(String grade) {
		if (grade != null) {
			String gradeInQuotes = "\"" + grade + "\"";
			if (allowedGrades.contains(gradeInQuotes)) {
				return true;
			}
		}
		return false;
	}

	public void setGrade(String grade) {
		if (isValidGrade(grade)) {
			this.grade = grade;
		} else if (grade == null || grade.isEmpty()) {
			this.grade = "none";
		} else {
			this.grade = NO_GRADE;
		}
	}

	@JsonIgnore
	@BsonIgnore
	public Integer getImportance() {
		switch (grade) {
		case "none":
			return 0;
		case "low":
			return 1;
		case "medium":
			return 2;
		case "high":
			return 3;
		case "critical":
			return 4;
		default:
			return null;
		}
	}
	
	public String getName() {
		if (name == null) {
			return key;
		}
		return name;
	}

//	public Date getCreatedFromObjectId() {
//		if (id != null) {
//			return id.getDate();
//		}
//		return null;
//	}

	public GraphElement() {
		this.key = (id == null ? null : id.toHexString());
		this.type = this.getClass().getName();
		this.simpleType = this.getClass().getSimpleName();
		this.name = this.key;
		this.weight = 1.0d;
		this.created = new Date();
		this.lastModified = this.created;
	}

	public GraphElement(String key) {
		this();
		setKey(key);
		setName(key);
	}

	public GraphElement(String key, String type) {
		this(key);
		setType(type);
	}

	public GraphElement(String key, Double weight) {
		this(key);
		setWeight(weight);
	}

	public GraphElement(String key, String type, Double weight) {
		this(key, type);
		setWeight(weight);
	}

//	public <T extends GraphElement> T deepCopy(Class<T> type) throws Exception {
//		ObjectMapper objectMapper = new ObjectMapper();
//		return (T) objectMapper.readValue(objectMapper.writeValueAsString(this), type);
//	}

	protected void checkWeight(Double weight, Double min, Double max) {
		if (weight == null || weight < min || weight > max) {
			String type = getType();
			String template = "%s must be between %s and %s inclusive, was %s";
			String msg = String.format(template, type, min, max, weight);
			throw new IllegalArgumentException(msg);
		}
	}

}
