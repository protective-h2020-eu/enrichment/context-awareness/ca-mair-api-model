package eu.h2020.protective.ca.mair.api.model.assets;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel(value = "Application")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.assets.Application")
public class Application extends Software {

	public Application() {
		super();
	}

	public Application(String key) {
		super(key);
	}

}

