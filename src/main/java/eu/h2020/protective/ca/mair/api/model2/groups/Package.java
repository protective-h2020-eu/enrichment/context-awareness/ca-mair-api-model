package eu.h2020.protective.ca.mair.api.model2.groups;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@ApiModel(value = "Package")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model2.groups.Package")
public class Package extends Group<Software> {

	public Package() {
		super();
	}

	public Package(String key) {
		super(key);
	}

}

