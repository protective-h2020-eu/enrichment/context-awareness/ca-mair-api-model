package eu.h2020.protective.ca.mair.api.model.assets;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.assets.Network")
public class Network extends Asset {

	@ApiModelProperty(required = false, value = "IPv4 subnet")
	private String ipV4Subnet;

	@ApiModelProperty(required = false, value = "IPv6 subnet")
	private String ipV6Subnet;

	@ApiModelProperty(required = false, value = "IPv4 subnet mask")
	private String ipV4SubnetMask;

	@ApiModelProperty(required = false, value = "IPv6 subnet mask")
	private String ipV6SubnetMask;

	public Network() {
		super();
	}

	public Network(String key) {
		super(key);
	}

}

