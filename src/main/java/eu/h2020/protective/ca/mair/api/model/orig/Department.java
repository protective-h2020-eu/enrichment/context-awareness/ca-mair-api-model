package eu.h2020.protective.ca.mair.api.model.orig;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.core.Vertex;
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model.orig.Department")
public class Department extends Vertex {

	public Department() {
		super();
	}

	public Department(String key) {
		super(key);
	}

}

