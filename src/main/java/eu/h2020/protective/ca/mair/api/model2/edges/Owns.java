package eu.h2020.protective.ca.mair.api.model2.edges;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model2.other.BusinessUnit;
import eu.h2020.protective.ca.mair.api.model2.other.MissionObjective;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@ApiModel
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model2.edges.Owns")
public class Owns extends Edge {

	public Owns(BusinessUnit from, MissionObjective to) {
		super(from, to);
	}

}

