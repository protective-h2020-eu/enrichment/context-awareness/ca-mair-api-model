package eu.h2020.protective.ca.mair.api.model2.assets;

import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@ApiModel(value = "Asset")
@BsonDiscriminator(key="type", value="eu.h2020.protective.ca.mair.api.model2.assets.Asset")
public class Asset extends Vertex {

	// @ApiModelProperty(required = false, value = "User instance")
	// private User user;
	//
	// @ApiModelProperty(required = false, value = "Site instance")
	// private Site site;
	//
	// @ApiModelProperty(required = false, value = "Business Unit instance")
	// private BusinessUnit businessUnit;

	@ApiModelProperty(required = false, value = "Location of Asset")
	private String location;
	
	@ApiModelProperty(required = false, value = "Person responsible for technical support of Asset")
	private String technicalOwner;
	
	@ApiModelProperty(required = false, value = "Person responsible for use of Asset")
	private String businessOwner;
	
	@ApiModelProperty(required = false, value = "Description of Asset")
	private String description;
	
	public Asset() {
		super();
	}

	public Asset(String key) {
		super(key);
	}

}

